with (import <nixpkgs> {});
mkShell {
	buildInputs = [
		pkg-config
		meson
		ninja
		boost
		(opencv.override { enableGtk2 = true; })

		valgrind
		libsForQt5.kcachegrind
		graphviz

		linuxKernel.packages.linux_latest_libre.perf
		hotspot
	];
}
